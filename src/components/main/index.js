import React from 'react'
import queryString from 'query-string'

import 'ta-semantic-ui/semantic/dist/components/reset.css'
import 'ta-semantic-ui/semantic/dist/components/site.css'
import 'styles/App.css'

import ZurichCircleMap from '../zurichCircleMap/ZurichCircleMap.component.js'

import svgMap from '../zurichCircleMap/data/zhCircleMap.svg'
import mapData from '../zurichCircleMap/data/mapData.json'
import dataBeds from '../zurichCircleMap/data/beds-by-circle.json'
import airbnbData from '../zurichCircleMap/data/airbnb.csv'

export default class AppComponent extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      dataSource: mapData,
      dataBeds: dataBeds,
      mapSource: svgMap,
      dataRow: 'relative'
    }
    this.changeDataRow = this.changeDataRow.bind(this)
  }

  changeDataRow (e) {
    this.setState({
      dataRow: e.currentTarget.getAttribute('data-row')
    })
    console.log(this.state.dataRow)
  }

  render () {
    const dataSet = queryString.parse(location.search)
    return (
      <div className='index' >

        {dataSet.data === 'airbnb' &&
        <ZurichCircleMap
          dataSource={airbnbData}
          mapId='zh-quarter-map'
          mapTitle='Airbnb-Angebote für die Stadt Zürich'
          mapLead=''
          mapDescription=''
          selectionHint='Kreis'
          height={640}
          width={640}
          tooltipLines={2}
          mapSource={svgMap}
          mapDataYear={2016}
          sortProp='Der Kreisbewohner'
          absoluteUnit='Angebote'
          relativeUnit=' CHF'
          absoluteDataRow='Medianpreis'
          dataRow='Angebote'
          eventHandler={this.changeDataRow}
        />
        }
      </div>
    )
  }
}

AppComponent.defaultProps = {}
