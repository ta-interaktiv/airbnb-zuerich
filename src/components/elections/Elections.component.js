import React from 'react'
// import {timeParse,timeFormat} from 'd3-time-format'
// import {scaleLinear} from 'd3-scale'
// import {line, curveCatmullRom} from 'd3-shape'
// import {extent} from 'd3-array'
// import {select as d3select} from 'd3-selection'
// import {axisBottom as d3axisBottom, axisLeft as d3axisLeft} from 'd3-axis'
// import {interpolate} from 'd3-interpolate's
import {nest} from 'd3-collection'

import data from './data.json'

class Elections extends React.Component {
  constructor (props) {
    super(props)
    console.log(data)

    this.state = {
      filteredData: []
    }
  }

  componentDidMount () {
    let winners = []
    for (let i = 0; i < data.length; i++) {
      if (data[i].winningParty) {
        winners.push(data[i])
      }
    }
    this.setState({
      filteredData: winners
    })
  }

  render () {
    let nestedData = nest()
      .key(function (d) { return d.winningParty })
      .entries(data)

    console.log(nestedData)

    data.forEach(function (d, i) {
      d.partyCode = ''
      if (d.winningParty === 'Lab') {
        d.partyCode = 2
      }
      if (d.winningParty === 'Con') {
        d.partyCode = 1
      }
      if (d.winningParty === 'Lib Dem') {
        d.partyCode = 4
      }
      if (d.winningParty === 'Lab Co-op') {
        d.partyCode = 2
      }
      if (d.winningParty === 'UKIP') {
        d.partyCode = 6
      }
      if (d.winningParty === 'Green') {
        d.partyCode = 5
      }
      if (d.winningParty === 'SNP') {
        d.partyCode = 3
      }
      if (d.winningParty === 'DUP') {
        d.partyCode = 7
      }
      if (d.winningParty === 'PC') {
        d.partyCode = 7
      }
      if (d.winningParty === 'SF') {
        d.partyCode = 7
      }
      if (d.winningParty === 'Ind') {
        d.partyCode = 7
      }
      if (d.winningParty === 'Speaker') {
        d.partyCode = 7
      }
      if (d.partyCode === '') {
        d.partyCode = 8
      }
    })

    let output = data.map((entry, i) =>
      <tr key={i}><td>{entry.name}</td><td>{entry.winningParty}</td><td>{entry.partyCode}</td></tr>
    )

    // const transform = 'translate(' + margin.left + ',' + margin.top + ')'

    return (
      <div style={{padding: '5px'}}>
        <table>
          <tbody>
            {output}
          </tbody>
        </table>
      </div>
    )
  }
}

// Elections.propTypes = {}

export default Elections
