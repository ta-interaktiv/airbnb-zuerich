import React from 'react'
import PropTypes from 'prop-types'
import ReactSVG from 'react-svg'
import { scaleLinear } from 'd3-scale'
import { extent } from 'd3-array'
import { select as d3select } from 'd3-selection'

import './svgMap.styl'
import 'ta-semantic-ui/semantic/dist/components/icon.css'
import 'ta-semantic-ui/semantic/dist/components/segment.css'
import 'ta-semantic-ui/semantic/dist/components/label.css'
import 'ta-semantic-ui/semantic/dist/components/table.css'
import 'ta-semantic-ui/semantic/dist/components/button.css'
import 'ta-semantic-ui/semantic/dist/components/header.css'
import 'ta-semantic-ui/semantic/dist/components/statistic.css'

class SVGMap extends React.Component {
  constructor (props) {
    super(props)

    let dataExtent = extent(this.props.dataSource, d => +d[this.props.dataRow])

    this.colorScale = scaleLinear()
      .domain(dataExtent)
      .range(['#ffefb2', '#2a4659'])

    this.state = {
      extent: dataExtent,
      width: this.props.width,
      height: this.props.height,
      showTooltip: false,
      actualMapData: this.props.dataSource,
      dataMatched: false,
      dataRow: this.props.dataRow
    }

    this.mouseEntersMapPart = this.mouseEntersMapPart.bind(this)
    this.mouseLeavesMapPart = this.mouseLeavesMapPart.bind(this)
    this.moveToFront = this.moveToFront.bind(this)
  }

  componentDidMount () {
    // let svgElements = document.getElementsByTagName('path')
    if (('ontouchstart' in window) ||
      (navigator.maxTouchPoints > 0) ||
      (navigator.msMaxTouchPoints > 0)) {
      this.setState({
        touchDevice: true
      })
    }
  }

  niceNumber (nStr) {
    if (nStr.toString().length < 5) {
      return nStr
    }
    nStr += ''
    let x = nStr.split('.')
    let x1 = x[0]
    let x2 = x.length > 1 ? '.' + x[1] : ''
    let rgx = /(\d+)(\d{3})/
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + '\'' + '$2')
    }
    return x1 + x2
  }

  moveToFront () {
    d3select.prototype.moveToFront = function () {
      return this.each(function () {
        this.parentNode.appendChild(this)
      })
    }
  }

  mouseEntersMapPart (e) {
    let title = (e.getAttribute('id'))

    if (title === 'background' || title === 'see') {
      this.setState({
        showTooltip: false
      })
      this.mouseLeavesMapPart()
      return
    }

    if (this.state.tooltipTitle === title) {
      return
    }

    // console.log(e)

    this.setState({
      dataMatched: false
    })

    // DESKTOP AND TOUCH DEVICE SWITCH
    if (this.state.touchDevice) {
      this.setState({
        tooltipY: 100,
        tooltipFontSize: '0.8em'
      })
    } else {
      this.setState({
        tooltipY: 65,
        tooltipFontSize: '1em'
      })
    }

    let currentId = -1
    // loop data to get count here
    for (var i = 0; i < this.state.actualMapData.length; i++) {
      if ('Kreis-' + this.state.actualMapData[i]['id'] === title) {
        // console.log(this.state.actualMapData[i]['key'])
        currentId = title.replace('Kreis-', '') - 1
        this.setState({
          tooltipCount: this.state.actualMapData[i][this.props.dataRow],
          tooltipCountAbsolute: this.state.actualMapData[i][this.props.absoluteDataRow],
          dataMatched: true
        })
      }
    }

    // show tooltip here
    this.setState({
      showTooltip: true,
      tooltipX: e.getAttribute('cx'),
      tooltipTitle: this.state.actualMapData[currentId]['name']
    })
  }

  mouseLeavesMapPart () {
    // hide tooltip here
    this.setState({
      showTooltip: false,
      dataMatched: false,
      tooltipTitle: ''
    })
  }

  render () {
    const data = this.props.dataSource

    // to keep 'this' alive in the data.map function below
    const _self = this

    data.forEach(function (d) {
      /* */
    })

    return (

      <div style={{padding: '5px'}}>

        <div className='ui tiny dividing infographic header'>
          {this.props.mapTitle}
          <div className='sub header'>{this.props.mapLead}</div>
        </div>

        <ReactSVG
          path={this.props.mapSource}
          style={{marginTop: this.state.touchDevice ? '-10px' : '0px'}}
          className={this.props.mapClassName}
          callback={(svg) => {
            let svgElements = svg.getElementsByTagName('path')

            for (let c = 0; c < svgElements.length; c++) {
              // TODO loop data through and match with id
              let title = svgElements[c].getAttribute('id')
              // loop data to get count here
              for (let i = 0; i < this.state.actualMapData.length; i++) {
                if ('Kreis-' +
                  this.state.actualMapData[i]['id'] ===
                  title &&
                  title !==
                  'see' &&
                  title !==
                  'background') {
                  let hoverOpacity = this.state.tooltipTitle === title ? 6 : 2
                  let colorizeCount = this.state.actualMapData[i][this.props.dataRow]
                  d3select(svgElements[c])
                    .style('fill', this.colorScale(colorizeCount))
                  d3select(svgElements[c]).style('stroke', 'white')
                  d3select(svgElements[c]).style('opacity', 1)
                  d3select(svgElements[c])
                    .style('stroke-width',
                      this.state.showTooltip ? hoverOpacity : 2)
                  d3select(svgElements[c])
                    .style('z-index',
                      this.state.tooltipTitle === title ? 9999 : 0)
                  d3select(svgElements[c]).style('stroke-alignment', 'inner')
                }
              }
            }

            // ANNOTATION MAGIC

            // let langstrasse = document.getElementById('Langstrasse')
            // console.log(langstrasse.getBBox())
            // d3select('.annotation').style('top', langstrasse.getBBox().y +
            // 'px')

            // DESKTOP AND TOUCH DEVICE SWITCH
            if (('ontouchstart' in window) ||
              (navigator.maxTouchPoints > 0) ||
              (navigator.msMaxTouchPoints > 0)) {
              // Touch
              for (let t = 0; t < svgElements.length; t++) {
                svgElements[t].addEventListener('touchstart', (e) => {
                  let element = e.currentTarget
                  _self.mouseEntersMapPart(element)
                })
              }
            } else {
              // No Touch
              for (let i = 0; i < svgElements.length; i++) {
                svgElements[i].addEventListener('mouseenter', (e) => {
                  // console.log(e.currentTarget)
                  let element = e.currentTarget
                  if (element.id === 'background') {
                    _self.mouseLeavesMapPart(element)
                  } else {
                    _self.mouseEntersMapPart(element)
                  }
                })
              }
            }
          }
          }

        />

        <div className='rect legend'>
          <div className='min'>
            {this.niceNumber(this.state.extent[0].toFixed(0))}
            <svg width='14px' height='14px' style={{marginLeft: '5px'}}>
              <rect x='0'
                y='0'
                width='14px'
                height='14px'
                fill={this.colorScale(this.state.extent[0])} />
            </svg>
          </div>
          <div className='max'>
            {this.niceNumber(this.state.extent[1].toFixed(0))}
            <svg width='14px' height='14px' style={{marginLeft: '5px'}}>
              <rect x='0'
                y='0'
                width='14px'
                height='14px'
                fill={this.colorScale(this.state.extent[1])} />
            </svg>
          </div>
        </div>

        <div style={{
          minHeight: '50px',
          marginTop: this.state.touchDevice ? '0px' : '10px'
        }}>

          {!this.state.showTooltip &&
          <div
            style={{
              minHeight: '70px'
            }}
            className='ui vertical center aligned very fitted segment'>
            <div
              className='ui sub header'>
              {this.props.selectionHint} auswählen
              <img
                src={require('./arrow.svg')}
                width='30px'
                style={{
                  position: 'relative',
                  marginLeft: '3px',
                  top: '-4px'
                }}
              />
            </div>
          </div>
          }

          {this.state.showTooltip &&
          this.state.dataMatched &&
          this.state.tooltipTitle !==
          'see' &&
          this.state.tooltipTitle !==
          'background' &&

          <table
            className='ui very basic unstackable infographic table'
          >
            <tbody>
              <tr className=''>
                <td className='single line four wide right aligned'
              >
                  <i style={{display: this.state.touchDevice ? 'inline' : 'none'}}
                    className='blue icon remove circle mobile-only'
                    onClick={() => this.mouseLeavesMapPart()} />
                  {this.state.tooltipTitle}
                  <i style={{paddingRight: '0px', paddingLeft: '5px'}}
                    className='ui disabled icon arrow right' />
                </td>

                {
                this.props.tooltipLines === 2 &&
                <td>
                  <div className='ui two small brown statistics'>
                    <div className='ui statistic'>
                      <div className='value'>{this.niceNumber(
                        this.state.tooltipCount)}</div>
                      <div className='label'>{this.props.dataRow}</div>
                    </div>
                    <div className='ui statistic mobile hidden'>
                      <div className='value'>{this.niceNumber(
                        this.state.tooltipCountAbsolute)}{this.props.relativeUnit}</div>
                      <div className='label'>{this.props.absoluteDataRow}</div>
                    </div>
                  </div>
                </td>
              }

                {
                this.props.tooltipLines === 1 &&
                <td style={{
                  width: '50%',
                  textAlign: 'left',
                  paddingLeft: '0px'
                }}>
                  <span style={{borderBottom: '2px solid black'}}><strong>{this.state.tooltipCountAbsolute}{this.props.absoluteUnit}</strong></span>
                </td>
              }
              </tr>
            </tbody>
          </table>
          }
        </div>

        {this.props.mapDescription &&
        <p
          className='graphic legend'
        >
          <strong>Hinweis: </strong>{this.props.mapDescription}
        </p>
        }

        <div className='detail'>
          Quelle: INURA, Statistisches Amt Kanton Zürich (2016)
        </div>

      </div>

    )
  }
}

SVGMap.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  dataSource: PropTypes.array,
  mapSource: PropTypes.string,
  mapTitle: PropTypes.string,
  mapLead: PropTypes.string,
  absoluteUnit: PropTypes.string,
  mapDescription: PropTypes.string,
  mapClassName: PropTypes.string,
  mapId: PropTypes.string,
  readerMadeHisChoice: PropTypes.bool,
  tooltipWidth: PropTypes.number,
  tooltipHeight: PropTypes.number,
  tooltipFontSize: PropTypes.number,
  mapData: PropTypes.array,
  mapDataYear: PropTypes.number,
  tooltipLines: PropTypes.number,
  dataRow: PropTypes.string,
  selectionHint: PropTypes.string,
  eventHandler: PropTypes.func,
  absoluteDataRow: PropTypes.string,
  relativeUnit: PropTypes.string
}

export default SVGMap
