import React from 'react'
// import {timeParse,timeFormat} from 'd3-time-format'
import {scaleLinear} from 'd3-scale'
// import {line, curveCatmullRom} from 'd3-shape'
import {extent} from 'd3-array'
import {select as d3select} from 'd3-selection'
// import {axisBottom as d3axisBottom, axisLeft as d3axisLeft} from 'd3-axis'
// import {interpolate} from 'd3-interpolate's
// import {nest} from 'd3-collection'
import ReactSVG from 'react-svg'

import './svgMap.styl'
import 'ta-semantic-ui/semantic/dist/components/icon.css'
import 'ta-semantic-ui/semantic/dist/components/segment.css'
import 'ta-semantic-ui/semantic/dist/components/label.css'
import 'ta-semantic-ui/semantic/dist/components/table.css'
import 'ta-semantic-ui/semantic/dist/components/button.css'

class SVGMap extends React.Component {
  constructor (props) {
    super(props)

    const actualData = this.props.mapData

    let minMaxAmount = []

    for (let x = 0; x < actualData.length; x++) {
      minMaxAmount.push(actualData[x][this.props.dataRow])
    }

    let dataExtent = extent(minMaxAmount)
    // dataExtent = [0, 100]

    this.colorScale = scaleLinear()
      .domain(dataExtent)
      .range(['#CAE8F4', '#1D5F82'])

    this.state = {
      extent: extent(minMaxAmount),
      width: this.props.width,
      height: this.props.height,
      showTooltip: false,
      actualMapData: actualData,
      dataMatched: false,
      dataRow: this.props.dataRow
    }

    this.mouseEntersMapPart = this.mouseEntersMapPart.bind(this)
    this.mouseLeavesMapPart = this.mouseLeavesMapPart.bind(this)
    this.moveToFront = this.moveToFront.bind(this)
  }

  componentDidMount () {
    // let svgElements = document.getElementsByTagName('path')
    if (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)) {
      this.setState({
        touchDevice: true
      })
    }
  }

  niceNumber (nStr) {
    if (nStr.toString().length < 5) {
      return nStr
    }
    nStr += ''
    let x = nStr.split('.')
    let x1 = x[0]
    let x2 = x.length > 1 ? '.' + x[1] : ''
    let rgx = /(\d+)(\d{3})/
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + '\'' + '$2')
    }
    return x1 + x2
  }

  moveToFront () {
    d3select.prototype.moveToFront = function () {
      return this.each(function () {
        this.parentNode.appendChild(this)
      })
    }
  }

  mouseEntersMapPart (e) {
    let title = (e.getAttribute('id'))

    if (title === 'background' || title === 'see') {
      this.setState({
        showTooltip: false
      })
      this.mouseLeavesMapPart()
      return
    }

    if (this.state.tooltipTitle === title) {
      return
    }

    // console.log(e)

    this.setState({
      dataMatched: false
    })

    // DESKTOP AND TOUCH DEVICE SWITCH
    if (this.state.touchDevice) {
      this.setState({
        tooltipY: 100,
        tooltipFontSize: '0.8em'
      })
    } else {
      this.setState({
        tooltipY: 65,
        tooltipFontSize: '1em'
      })
    }

    // loop data to get count here
    for (var i = 0; i < this.state.actualMapData.length; i++) {
      if (this.state.actualMapData[i]['quarter'] === title) {
        // console.log(this.state.actualMapData[i]['key'])
        this.setState({
          tooltipCount: this.state.actualMapData[i][this.props.dataRow],
          tooltipCountAbsolute: this.state.actualMapData[i]['grund'],
          dataMatched: true
        })
      }
    }

    // show tooltip here
    this.setState({
      showTooltip: true,
      tooltipX: e.getAttribute('cx'),
      tooltipTitle: title
    })
  }

  mouseLeavesMapPart () {
    // hide tooltip here
    this.setState({
      showTooltip: false,
      dataMatched: false,
      tooltipTitle: ''
    })
  }

  render () {
    const data = this.props.dataSource

    // to keep 'this' alive in the data.map function below
    const _self = this

    // let margin = {top: 15, right: 20, bottom: 20, left: 35}
    // let w = this.state.width - (margin.left + margin.right)
    // let h = this.props.height - (margin.top + margin.bottom)

    // const percentageFormat = locale.format('')

    data.forEach(function (d) {
      /* */
    })

    // const transform = 'translate(' + margin.left + ',' + margin.top + ')'

    return (

      <div style={{padding: '5px'}}>

        { this.props.mapTitle &&
          <p
            style={{
              marginBottom: '0px',
              fontFamily: 'Benton Sans Cond',
              fontWeight: 'bold'
            }}
          >
            {this.props.mapTitle}
          </p>
        }
        { this.props.mapLead &&
          <p style={{marginBottom: '0px'}}>{this.props.mapLead}</p>
        }

        <div
          className='button-wrapper'
          style={{textAlign: 'center', marginTop: '10px'}}
        >
          <div
            className='ui small buttons'
          >
            <button
              className={`ui button ${this.props.dataRow === 'verheiratet' ? 'active' : ''}`}
              data-row='verheiratet'
              onClick={this.props.eventHandler}
            >
              Verheiratete
            </button>
            <button
              className={`ui button ${this.props.dataRow === 'grund' ? 'active' : ''}`}
              data-row='ground'
              onClick={this.props.eventHandler}
            >
              Ledige
            </button>
          </div>
        </div>

        <div style={{
          minHeight: '50px',
          marginTop: this.state.touchDevice ? '0px' : '10px'
        }}>

          { !this.state.showTooltip &&
            <div style={{verticalAlign: 'top', maxHeight: '50px', paddingTop: '5px', fontSize: '0.9em', color: '#5A686D'}} className='ui vertical center aligned very fitted segment'>
              <div className='ui header'>Quartier auswählen</div>
              <p style={{marginTop: '-5px'}}><i className='icon chevron down' /></p>
            </div>
          }

          { this.state.showTooltip && this.state.dataMatched && this.state.tooltipTitle !== 'see' && this.state.tooltipTitle !== 'background' &&

          <table
            className=''
            style={{
              position: 'relative',
              minHeight: '50px',
              top: '0px',
              width: '100%',
              left: '0px',
              fontSize: this.state.tooltipFontSize
            }}
          >
            <tbody>
              <tr>
                <td
                  style={{
                    textAlign: 'right',
                    width: '50% !important',
                    paddingRight: '10px'
                  }}
                >
                  <i style={{display: this.state.touchDevice ? 'inline' : 'none'}}
                    className='icon remove circle mobile-only'
                    onClick={() => this.mouseLeavesMapPart()} />
                  {this.state.tooltipTitle}
                  <i style={{paddingRight: '0px', paddingLeft: '5px'}}
                    className='ui icon arrow right' />
                </td>
                {this.props.tooltipLines === 2 &&
                  <td style={{width: '50%', textAlign: 'left', paddingLeft: '0px'}}>
                    <span style={{borderBottom: '2px solid black'}}><strong>{this.niceNumber(this.state.tooltipCount)} {this.props.absoluteUnit} </strong></span>
                    <br />{this.props.dataRow === 'verheiratet' ? 'Tarif für' +
                  ' Verheiratete' : 'Tarif für Ledige'}
                  </td>
                }
                {this.props.tooltipLines === 1 &&
                  <td style={{width: '50%', textAlign: 'left', paddingLeft: '0px'}}>
                    <span style={{borderBottom: '2px solid black'}}><strong>{this.state.tooltipCountAbsolute} {this.props.absoluteUnit}</strong></span>
                  </td>
                }
              </tr>
            </tbody>
          </table>
        }
        </div>

        <ReactSVG
          path={this.props.mapSource}
          style={{marginTop: this.state.touchDevice ? '-10px' : '0px'}}
          className={this.props.mapClassName}
          callback={(svg) => {
            let svgElements = svg.getElementsByTagName('path')

            for (let c = 0; c < svgElements.length; c++) {
              // TODO loop data through and match with id
              let title = svgElements[c].getAttribute('id')
              // loop data to get count here
              for (let i = 0; i < this.state.actualMapData.length; i++) {
                if (this.state.actualMapData[i]['quarter'] === title && title !== 'see' && title !== 'background') {
                  let hoverOpacity = this.state.tooltipTitle === title ? 6 : 2
                  let colorizeCount = this.state.actualMapData[i][this.props.dataRow]
                  d3select(svgElements[c]).style('fill', this.colorScale(colorizeCount))
                  d3select(svgElements[c]).style('stroke', this.state.tooltipTitle === title ? 'white' : 'white')
                  d3select(svgElements[c]).style('opacity', this.state.showTooltip ? 1 : 1)
                  d3select(svgElements[c]).style('stroke-width', this.state.showTooltip ? hoverOpacity : 2)
                  d3select(svgElements[c]).style('z-index', this.state.tooltipTitle === title ? 9999 : 0)
                  d3select(svgElements[c]).style('stroke-alignment', 'inner')
                }
              }
            }

            // ANNOTATION MAGIC

            // let langstrasse = document.getElementById('Langstrasse')
            // console.log(langstrasse.getBBox())
            // d3select('.annotation').style('top', langstrasse.getBBox().y +
            // 'px')

            // DESKTOP AND TOUCH DEVICE SWITCH
            if (('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)) {
              // Touch
              for (let t = 0; t < svgElements.length; t++) {
                svgElements[t].addEventListener('touchstart', (e) => {
                  let element = e.currentTarget
                  _self.mouseEntersMapPart(element)
                })
              }
            } else {
              // No Touch
              for (let i = 0; i < svgElements.length; i++) {
                svgElements[i].addEventListener('mouseenter', (e) => {
                  // console.log(e.currentTarget)
                  let element = e.currentTarget
                  if (element.id === 'background') {
                    _self.mouseLeavesMapPart(element)
                  } else {
                    _self.mouseEntersMapPart(element)
                  }
                })
              }
            }
          }
          }

        />

        <div className='rect legend'>
          <div className='min'>
            {this.niceNumber(this.state.extent[0].toFixed(0))} {this.props.absoluteUnit}
            <svg width='14px' height='14px' style={{marginLeft: '5px'}}>
              <rect x='0' y='0' width='14px' height='14px' fill={this.colorScale(this.state.extent[0])} />
            </svg>
          </div>
          <div className='max'>
            {this.niceNumber(this.state.extent[1].toFixed(0))} {this.props.absoluteUnit}
            <svg width='14px' height='14px' style={{marginLeft: '5px'}}>
              <rect x='0' y='0' width='14px' height='14px' fill={this.colorScale(this.state.extent[1])} />
            </svg>
          </div>
        </div>

        {this.props.mapDescription &&
          <p
            className='graphic legend'
            style={{display: 'none'}}
          >
            <strong>Hinweis: </strong>{this.props.mapDescription}
          </p>
        }

        <p
          style={{textAlign: 'center', fontFamily: 'Benton Sans Cond', paddingTop: '15px', fontSize: '0.9em'}}
        >
          Daten:&nbsp;&nbsp;
          <a
            className='ui label'
            href='https://www.stadt-zuerich.ch/prd/de/index/statistik/themen/wirtschaft/einkommen-vermoegen.html' target='_blank'
          >
            <i
              className='external icon'
            />
            Stadt Zürich
          </a>
        </p>

      </div>

    )
  }
}

SVGMap.propTypes = {
  width: React.PropTypes.number,
  height: React.PropTypes.number,
  dataSource: React.PropTypes.array,
  mapSource: React.PropTypes.string,
  mapTitle: React.PropTypes.string,
  mapLead: React.PropTypes.string,
  absoluteUnit: React.PropTypes.string,
  mapDescription: React.PropTypes.string,
  mapClassName: React.PropTypes.string,
  mapId: React.PropTypes.string,
  readerMadeHisChoice: React.PropTypes.bool,
  tooltipWidth: React.PropTypes.number,
  tooltipHeight: React.PropTypes.number,
  tooltipFontSize: React.PropTypes.number,
  mapData: React.PropTypes.array,
  mapDataYear: React.PropTypes.number,
  tooltipLines: React.PropTypes.number,
  dataRow: React.PropTypes.string,
  eventHandler: React.PropTypes.function
}

export default SVGMap
