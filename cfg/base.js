'use strict'
let path = require('path')
let defaultSettings = require('./defaults')

const dfltPort = 8000

// Additional npm or bower modules to include in builds
// Add all foreign plugins you may need into this array
// @example:
// let npmBase = path.join(__dirname, '../node_modules');
// let additionalPaths = [ path.join(npmBase, 'react-bootstrap') ];
let additionalPaths = []

module.exports = {
  devtool: 'eval',
  devServer: {
    contentBase: './src/',
    port: defaultSettings.port,
    historyApiFallback: true,
    hot: true,
    publicPath: defaultSettings.devPublicPath,
    noInfo: false,
    stats: 'minimal'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    alias: {
      actions: `${defaultSettings.srcPath}/actions/`,
      components: `${defaultSettings.srcPath}/components/`,
      sources: `${defaultSettings.srcPath}/sources/`,
      stores: `${defaultSettings.srcPath}/stores/`,
      styles: `${defaultSettings.srcPath}/styles/`,
      config: `${defaultSettings.srcPath}/config/` + process.env.REACT_WEBPACK_ENV
    }
  }
}
