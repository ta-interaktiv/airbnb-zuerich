'use strict'
const path = require('path')
const srcPath = path.join(__dirname, '/../src')
const webpack = require('webpack')
// Common Plugins
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const cssTextExtractor = new ExtractTextPlugin({
  filename: '[name].[contenthash].css',
  disable: process.env.REACT_WEBPACK_ENV === 'dev'
})

// Provide default compression settings
const imageCompressionSettings = {
  mozjpeg: {
    quality: 65,
    progressive: false
  },
  pngquant: {
    quality: '65-90',
    speed: 4
  },
  svgo: {
    plugins: [
      {
        removeViewBox: false
      },
      {
        removeEmptyAttrs: false
      }
    ]
  },
  gifsicle: {
    optimizationLevel: 7,
    interlaced: false
  },
  optipng: {
    optimizationLevel: 7,
    interlaced: false
  }
}

/**
 * CSS Loader options
 * @type {{importLoaders: number, minimize: {discardComments: {removeAll: boolean}, discardOverriden: boolean, discardDuplicates: boolean}}}
 */
const cssLoaderOptions = {
  importLoaders: 1,
  minimize: {
    discardComments: {removeAll: true},
    discardOverriden: true,
    discardDuplicates: true,
    mergeLonghand: true,
    colormin: true
  }
}

function getDefaultModules () {
  return {
    rules: [
      {
        test: /\.(js|jsx)$/,
        enforce: 'pre',
        use: ['eslint-loader']
      },
      {
        test: /\.css$/,
        use: cssTextExtractor.extract({
          use: [
            {
              loader: 'css-loader',
              options: cssLoaderOptions
            },
            'postcss-loader'
          ],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.sass/,
        use: cssTextExtractor.extract({
          use: [
            {
              loader: 'css-loader',
              options: cssLoaderOptions
            },
            'postcss-loader',
            {
              loader: 'sass-loader',
              options: {
                outputStyle: 'expanded',
                indentedSyntax: true
              }
            }
          ],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.scss/,
        use: cssTextExtractor.extract({
          use: [
            {
              loader: 'css-loader',
              options: cssLoaderOptions
            },
            'postcss-loader',
            'sass-loader'
          ],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.less/,
        use: cssTextExtractor.extract({
          use: [
            {
              loader: 'css-loader',
              options: cssLoaderOptions
            },
            'postcss-loader',
            'less-loader'
          ],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.styl/,
        use: cssTextExtractor.extract({
          use: [
            {
              loader: 'css-loader',
              options: cssLoaderOptions
            },
            'stylus-loader'
          ],
          fallback: 'style-loader'
        })
      },
      {
        test: /\.(woff2?|mp\d|mpe?g|mov|webm|ogv|wav|aiff?|m4[av])$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)/i,
        use: [
          // Use the srcset-loader. If you append your image URLs with a
          // querystring, you will get a srcset-loader object, otherwise you
          // will get a normal source string.
          'srcset-loader',
          {
            loader: 'url-loader',
            options: {
              limit: 8192
            }
          },
          {
            loader: 'image-webpack-loader',
            options: imageCompressionSettings
          }

        ]
      },

      {
        test: /\.csv/,
        use: ['dsv-loader']
      }
    ]
  }
}

function getDefaultPlugins () {
  return [
    new webpack.NoEmitOnErrorsPlugin(),
    cssTextExtractor
  ]
}

module.exports = {
  srcPath: srcPath,
  publicPath: 'assets/',
  devPublicPath: '/assets/',
  port: 8000,
  getDefaultModules: getDefaultModules,
  getDefaultPlugins: getDefaultPlugins
}
