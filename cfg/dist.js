'use strict'

let path = require('path')
let webpack = require('webpack')

let baseConfig = require('./base')
let defaultSettings = require('./defaults')

/**
 * Project configuration: A JSON file that contains all the meta data used
 * to generate the index.html. Needs to be adapted for every project.
 */
let projectConfig = require('../src/config.json')

// Add needed plugins here
let HtmlWebpackPlugin = require('html-webpack-plugin')
let ChunkManifestPlugin = require('chunk-manifest-webpack-plugin')
let WebpackChunkHash = require('webpack-chunk-hash')

let config = Object.assign({}, baseConfig, {
  entry: {
    main: path.join(__dirname, '../src/index')
  },
  cache: false,
  devtool: 'source-map',
  output: {
    path: path.join(__dirname, '/../dist/assets'),
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].js',
    publicPath: defaultSettings.publicPath
  },
  plugins: [
    new HtmlWebpackPlugin(Object.assign({}, projectConfig, {
      template: 'src/index.ejs',
      filename: '../index.html'
    })),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        drop_console: true,
        passes: 2
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module) {
        return module.context && module.context.indexOf('node_modules') !== -1
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest'
    }),
    new WebpackChunkHash(),
    new ChunkManifestPlugin({
      filename: 'chunk-manifest.json',
      manifestVariable: 'webpackManifest',
      inlineManifest: true
    })

  ],
  module: defaultSettings.getDefaultModules()
})

// Add needed loaders to the defaults here
config.module.rules.push({
  test: /\.(js|jsx)$/,
  loader: 'babel-loader',
  include: [].concat(
    [path.join(__dirname, '/../src')]
  )
})

config.plugins = config.plugins.concat(defaultSettings.getDefaultPlugins())

// Add alias to react lite
//config.resolve.alias = Object.assign({}, config.resolve.alias, {
//  'react': 'react-lite',
//  'react-dom': 'react-lite'
//})

module.exports = config
