# Changelog React Base Project

## Upcoming

## 0.6.1 - 2017-06-01
### 🐛 Fixes
- Changed HTML template to request the DimensionWatcher script using HTTPS.

## 0.6.0 – 2017-05-15
### Changes
- 🔧 Added image-webpack-loader, so images will now be automatically optimised.
- 🔧 Also added [srcset-loader](https://github.com/timse/srcset-loader) for additional benefits, like creating [srcSets](https://css-tricks.com/responsive-images-youre-just-changing-resolutions-use-srcset/) or placeholder images.
- 🔧 Added CSS extraction, so styles will reside in their own CSS file (which can be downloaded in parallel to the JavaScript).
- 🔧 Added vendor code splitting, so libraries will stay in their own JS file and can be cached.
- 🔼 Updated dependencies

## 0.5.1 – 2017-05-03
### Changes
- 🔧 Use Standard style, but with eslint
- ➕ Updated dependencies

## 0.5.0 - 2017-04-28
### Changes
- 🔧 Switched from a generic eslint config to [Standard JS Style](https://standardjs.com/). See Readme for instructions.
- 🔧 Enabled git-lfs. See Readme for instructions.

## 0.4.5 - 2017-04-19
### Changes
- 🔼 Update Webpack to Webpack 2
- 🔧 Updated config to work with Webpack 2

## 0.4.4 - 2017-03-02
### Changes
- Replace internal copyright notice component with ta-react-copyright-notice
- Replace internal standalone share buttons component with 
ta-react-share-buttons


## 0.4.3 – 2017-01-28
### Changes
- Upgraded various dependencies
- Added Postcss-Autoprefixer and browserlist with a list of browsers we 
should support


## 0.4.2 – 2017-01-17
### Fixes
- Actually upgrading the ta-react components to the newest versions.

## 0.4.1 – 2017-01-16
### Fixes
- Double apostrophes only generate an eslint warning instead of a 
build-breaking error.

### Changes
- Updated React-Lite dependency
- Tried to update the ta-react component dependencies, but failed, apparently.

## 0.4.0 – 2016-12-01
### Changes
- Replace internal masthead component with ta-react-masthead
- Replace internal feedback message component with ta-react-feedback-message

## 0.3.1 – 2016-11-25
### Fixes
- Use React 15.4 and React-Hot-Loader 3.0
- Clean up console calls during build

## 0.3.0 - 2016-11-02
### New

- Replaced all internal tracking components with [ta-react-tracking](https://www.npmjs.com/package/ta-react-tracking).

## 0.2.0 – 2016-10-19
### Important
- Changed version numbering scheme: This base project will only use 0.x.x 
version numbers. Final apps that base on this project are advised to use x.x
.x version numbers – this way, those two ranges shouldn't clash.

### New
- Added yarn.lock file
- Added .npmrc file to stop `npm/yarn version` to tag the version.
- Use `useDimensionWatcher` flag in `src/config.json` in order to include the
 DimensionWatcher script in your `index.html`.
- Use a fancy dashboard when running `npm run start`. Normal output is used 
when running `npm run serve`.

### Changed


## 1.1.1 - 2016-08-23
### Changed
- Fixed dev config, so it won't throw as many errors in console anymore

### New
- Added readme and changelog.

## 1.1.0 - 2016-08-11
### New
- Default loader for CSV files. Based on D3-dsv, so it provides neatly organised JSON objects by default.

## 1.0.1 - 2016-08-11
### Changed
- Fix in config so that the dev server works again.

## 1.0.0 - 2016-08-10

First release; includes a base project with the following components:

- Masthead
- Copyright notice
- Analytics/Trackers
- Standalone Share Buttons
- Feedback Notice
